# -*- coding: utf-8 -*-
"""Azure Translator module."""
import xml.etree.ElementTree as ET
import datetime
from urllib import urlencode

import requests
from .errors import (
    AzureApiError, AzureApiBadFormatError,
    AzureCannotGetTokenError, AzureApiTimeoutError
)
from api import *


class Translator(object):
    """
    Translator class, connects to Azure Translator API:

        http://docs.microsofttranslator.com/text-translate.html
    """
    DEFAULT_LANGUAGE = 'es'

    def __init__(self):
        self.api_key = None
        self._access_token = None
        self._access_token_time = None

    def init_app(self, app):
        self.api_key = app.config['AZURE_TRANSLATOR']['API_KEY']

    @property
    def access_token(self):
        """
            Retrieve an access token in order to use the Translator API.
        """

        if self._access_token_time:
            if (datetime.datetime.now() - self._access_token_time).total_seconds()/60 < 9:
                return self._access_token
        try:
            resp = requests.post(
                TOKEN_ENDPOINT,
                headers={
                    'Content-Type': 'application/json',
                    'Accept': 'application/jwt',
                    'Ocp-Apim-Subscription-Key': self.api_key,
                },
                timeout=HTTP_TIMEOUT
            )
            resp.raise_for_status()
        except requests.exceptions.Timeout as error:
            raise AzureApiTimeoutError(unicode(error), request=error.request)
        except requests.exceptions.HTTPError as error:
            raise AzureCannotGetTokenError(
                unicode(error),
                response=error.response,
                request=error.request
            )
        self._access_token_time = datetime.datetime.now()
        self._access_token = resp.content
        return self._access_token

    def _translate_text(self, text, to=DEFAULT_LANGUAGE, source_language=None):
        """
            Translate some text into some language. Target language default to english.

            :param text:               the text to translate
            :param to:                 the target language
            :param source_language:    optional source language
            """
        params = {
            'text': self._clean_translatable(text),
            'to': to,
        }
        if source_language:
            params['from'] = source_language
        try:
            resp = requests.get(
                TRANSLATE_ENDPOINT,
                headers={
                    'Authorization': 'Bearer {}'.format(self.get_access_token()),
                    'Accept': 'application/xml',
                },
                params=params,
                timeout=HTTP_TIMEOUT
            )
            resp.raise_for_status()
        except requests.exceptions.Timeout as error:
            raise AzureApiTimeoutError(unicode(error), request=error.request)
        except requests.exceptions.HTTPError as error:
            raise AzureApiError(
                unicode(error),
                response=error.response,
                request=error.request
            )

        try:
            return ET.fromstring(resp.content).text
        except ET.ParseError as e:
            raise AzureApiBadFormatError(unicode(e), response=resp, request=getattr(resp, 'request', None))

    def _translate_array(self, array, to=DEFAULT_LANGUAGE, source_language=None):
        """
            Translate some text into some language. Target language default to english.

            :param array:              array of texts to translate
            :param to:                 the target language
            :param source_language:    optional source language
            """
        data = self._build_translate_array_request(array, to, source_language)
        try:
            resp = requests.post(
                TRANSLATE_ARRAY_ENDPOINT,
                headers={
                    'Authorization': 'Bearer {}'.format(self.access_token),
                    'Accept': 'application/xml',
                    'Content-Type': 'application/xml'
                },
                data=data,
                timeout=HTTP_TIMEOUT
            )
            resp.raise_for_status()
        except requests.exceptions.Timeout as error:
            raise AzureApiTimeoutError(unicode(error), request=error.request)
        except requests.exceptions.HTTPError as error:
            raise AzureApiError(
                unicode(error),
                response=error.response,
                request=error.request,
                data=data
            )

        try:
            translations = ET.fromstring(resp.content)
            return map(lambda x: x.find('ns1:TranslatedText', RESPONSE_NAMESPACES).text, translations)
        except ET.ParseError as e:
            raise AzureApiBadFormatError(unicode(e), response=resp, request=getattr(resp, 'request', None))

    def _build_translate_array_request(self, array, to=DEFAULT_LANGUAGE, source_language=None):
        texts = reduce(
            lambda x, y: x + y,
            map(
                lambda t: TRANSLATE_ARRAY_REQUEST_VALUE % self._clean_translatable(t),
                array))

        request = TRANSLATE_ARRAY_REQUEST % {'to': to, 'texts': texts}
        if source_language:
            from_lang = '<From>%s</From>' % source_language
            request.replace('<Texts>', from_lang + '<Texts>')
        return unicode(request).encode('utf-8')

    def _clean_translatable(self, translatable):
        return translatable.replace('&', '&#038;')

    def translate(self, translatable, to=DEFAULT_LANGUAGE, source_language=None):
        if isinstance(translatable, basestring):
            return self._translate_text(translatable, to, source_language)
        return self._translate_array(translatable, to, source_language)
